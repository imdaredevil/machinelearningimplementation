
import numpy as np


def stepFunction(value):
	if value>0:
		return 1
	else:
		return 0


class SingleLayerPerceptron:

	def __init__(self,learningRate,numberOfFeatures,numberOfOutputs,activationFunction):
		self.learningRate = learningRate
		self.weights = np.zeros((numberOfOutputs,numberOfFeatures + 1)) # additional weight for bias
		self.activationFunction = activationFunction
		

	def fit(self,trainingData,trainingOutput,iterations):
		trainingOutput  = np.array(trainingOutput)
		trainingOutput = trainingOutput.reshape((len(trainingOutput),len(self.weights)))
		trainingData   = np.array(trainingData)
		length = len(trainingOutput)
		if length != len(trainingData):
			raise Exception("the number of records in input data and output are not matching")
		for k in range(iterations):
			for i in range(length):
				predictedOutput = self.__predictRow(trainingData[i])
				error = self.__CalculateError(predictedOutput,trainingOutput[i])
				self.__ModifyWeights(error,trainingData[i])


	def __predictRow(self,input):

		input = np.append(input,[-1])
		#print(input)
		predictedOutput = np.zeros((len(self.weights)))
		for j in range(len(predictedOutput)):
			sum = 0
			for i in range(len(input)):
				sum = sum  + input[i]*self.weights[j][i]

			predictedOutput[j] = self.activationFunction(sum)
		#print(str(sum) + " " + str(predictedOutput))

		return predictedOutput


	def __CalculateError(self,predictedOutput,actualOutput):

		#print(actualOutput)
		error = np.zeros(np.shape(predictedOutput))
		for i in range(len(predictedOutput)):
			error[i] = (actualOutput[i] - predictedOutput[i])
		print(error)
		return error

	def __ModifyWeights(self,error,input):

		input = np.append(input,[-1])
		for j in range(len(self.weights)):

			diff = error[j]*self.learningRate
			#print(diff,end = ' ')
			for i in range(len(self.weights[j])):
				self.weights[j][i] = self.weights[j][i] + diff*input[i]
		print(self.weights)


	def predict(self,testData):

		return [self.__predictRow(testDataRow) for testDataRow in testData]

	def predictSingle(self,x):

		return self.__predictRow(x)










		
